const express = require('express')
const router = express.Router()
const User = require('../models/User')
const { generateAccessToken } = require('../helper/auth')
const bcrypt = require('bcryptjs/dist/bcrypt')

const login = async function (req, res, next) {
  const username = req.body.username
  const password = req.body.password
  try {
    const user = await User.findOne({ username: username }).exec()
    const verifyResult = await bcrypt.compare(password, user.password)
    if (!verifyResult) {
      return res.status(404).send({
        message: 'User not found!!'
      })
    }
    const token = generateAccessToken({ _id: user._id, username: user.username })

    res.json({ user: { _id: user._id, username: user.username, roles: user.roles }, token: token })
  } catch (err) {
    console.log(err)
    return res.status(404).send({
      message: err.message
    })
  }
}

router.post('/login', login)

module.exports = router
